package schlosstorgelow.de.timerappv4.ui;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.DBObject;
import schlosstorgelow.de.timerappv4.obj.Group;
import schlosstorgelow.de.timerappv4.obj.Mark;

public class CreateGroupFragment extends Fragment implements CreateActivity.CreateActivityListener {
    private EditText name;
    private EditText weight;

    private TextView percentage;

    private TextInputLayout inputLayout_name;
    private TextInputLayout inputLayout_weight;

    private DBObject parent;
    private double allWeightOfParentGroup;


    public CreateGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        parent = (DBObject) getArguments().getSerializable("parent");
        if (parent == null) {
            throw new RuntimeException(this.toString() + " needs to be passed a parent!");
        }

        for (Group g : DBHelper.readGroupsByParentID(parent.getHierarchyId())) {
            allWeightOfParentGroup += g.getWeight();
        }
        for (Mark m : DBHelper.readMarksByParentID(parent.getHierarchyId())) {
            allWeightOfParentGroup += m.getWeight();
        }

        View v = inflater.inflate(R.layout.fragment_create_group, container, false);
        name = (EditText) v.findViewById(R.id.editText_name);
        weight = (EditText) v.findViewById(R.id.editText_weight);

        percentage = (TextView) v.findViewById(R.id.percentage);

        inputLayout_name = (TextInputLayout) v.findViewById(R.id.input_view_name);
        inputLayout_weight = (TextInputLayout) v.findViewById(R.id.input_view_weight);

        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validateName();
                }
            }
        });
        weight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validateWeight();
                }
            }
        });
        weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                double input = 0;
                if (!(s.length() == 0)) {
                    input = Double.parseDouble(weight.getText().toString());
                    percentage.setText(String.valueOf((double) Math.round((input / (allWeightOfParentGroup + input)) * 10000) / 100.00) + "%");
                }

            }
        });


        return v;
    }

    public boolean validateWeight() {
        boolean inputOK = false;
        double input = 0;
        if (weight.getText().length() == 0) {
            inputLayout_weight.setErrorEnabled(true);
            inputLayout_weight.setError("Weight can't be empty!");
        } else {
            input = Double.parseDouble(weight.getText().toString());
            if (input <= 0) {
                inputLayout_weight.setErrorEnabled(true);
                inputLayout_weight.setError("Weight must be greater that zero!");
            } else {
                inputLayout_weight.setErrorEnabled(false);
                inputOK = true;
            }
        }


        return inputOK;

    }


    public boolean validateName() {
        boolean inputOK = false;
        String input = name.getText().toString();
        if (input.length() == 0) {
            inputLayout_name.setErrorEnabled(true);
            inputLayout_name.setError("Name can't be empty!");
        } else if (input.length() > 40) {
            inputLayout_name.setErrorEnabled(true);
            inputLayout_name.setError("Name can't be longer that 40 Symbols!");
        } else {
            inputLayout_name.setErrorEnabled(false);
            inputOK = true;
        }

        return inputOK;
    }

    public boolean save(DBObject parent) {

        if (validateName() && validateWeight()) {
            String _name = name.getText().toString();
            double _weight = Double.parseDouble(weight.getText().toString());
            DBHelper.createGroup(new Group(parent.getHierarchyId(), _name, _weight));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public DBObject getParent() {
        return parent;
    }

}
