package schlosstorgelow.de.timerappv4.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.Group;

public class UpdateGroupFragment extends Fragment {

    private EditText name;
    private EditText weight;

    private Button save;
    private Button delete;

    private Group groupToUpdate;

    public UpdateGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_update_group, container, false);

        name = (EditText) v.findViewById(R.id.update_group_editText_name);
        weight = (EditText) v.findViewById(R.id.update_group_editText_weight);
        save = (Button) v.findViewById(R.id.update_group_save);
        delete = (Button) v.findViewById(R.id.update_group_delete);

        groupToUpdate = (Group) getArguments().getSerializable("group");

        name.setText(groupToUpdate.getName());
        weight.setText(String.valueOf(groupToUpdate.getWeight()));

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .title("Delete Group " + groupToUpdate.getName())
                        .content("Are you sure you want to delete this Group?")
                        .positiveText("OK!")
                        .negativeText("Cancel!")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                if (DBHelper.deleteGroup(groupToUpdate)) {
                                    new MaterialDialog.Builder(getActivity())
                                            .title("Delete Successful!")
                                            .content(groupToUpdate.getName() + " was deleted!")
                                            .positiveText("OK!")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                                    getActivity().onBackPressed();
                                                }
                                            })
                                            .show();
                                }
                            }
                        })
                        .show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groupToUpdate.setName(name.getText().toString());
                groupToUpdate.setWeight(Double.parseDouble(weight.getText().toString()));
                if (DBHelper.updateGroup(groupToUpdate)) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Sucessfull updated!")
                            .content(groupToUpdate.getName() + " was updated!")
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                    getActivity().onBackPressed();
                                }
                            })
                            .show();
                }
            }
        });

        return v;
    }

    public boolean validateWeight() {
        boolean inputOK = false;
        double input = 0;
        if (weight.getText().length() == 0) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Weight cant be empty!")
                    .positiveText("OK!")
                    .show();
        } else {
            input = Double.parseDouble(weight.getText().toString());
            if (input <= 0) {
                new MaterialDialog.Builder(getActivity())
                        .title("Input Error")
                        .content("Weight must be greater than zero!")
                        .positiveText("OK!")
                        .show();
            } else {
                inputOK = true;
            }
        }


        return inputOK;

    }

    public boolean validateName() {
        boolean inputOK = false;
        String input = name.getText().toString();
        if (input.length() == 0) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Name cant be empty!")
                    .positiveText("OK!")
                    .show();
        } else if (input.length() > 40) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Name cant be longer tham 40 Symbols!")
                    .positiveText("OK!")
                    .show();
        } else {
            inputOK = true;
        }

        return inputOK;
    }
}
