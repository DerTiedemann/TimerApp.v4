package schlosstorgelow.de.timerappv4.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.obj.Mark;

/**
 * Created by Jan Max on 17.12.2015.
 */
public class MarkHolder extends TreeNode.BaseNodeViewHolder<Mark> {
    private MarkHolderListener mListener;

    public MarkHolder(Context context) {
        super(context);
        if (context instanceof MarkHolderListener) {
            mListener = (MarkHolderListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MarkHolderListener");
        }

    }

    @Override
    public View createNodeView(TreeNode node, final Mark m) {
        final LayoutInflater lf = LayoutInflater.from(context);
        final View v = lf.inflate(R.layout.tree_mark, null, false);

        TextView name = (TextView) v.findViewById(R.id.tree_mark_name);
        TextView value = (TextView) v.findViewById(R.id.tree_mark_value);
        TextView weight = (TextView) v.findViewById(R.id.tree_mark_weight);

        name.setText(m.getName());
        value.setText(String.valueOf(m.getValue()));
        weight.setText(String.valueOf(m.getWeight()));

        ImageButton info = (ImageButton) v.findViewById(R.id.tree_mark_info);

        Resources r = mListener.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, node.getLevel() * (node.getLevel() > 1 ? r.getInteger(R.integer.tree_margin) : 0), r.getDisplayMetrics());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = px;
        v.setLayoutParams(params);

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openMarkUpdate(m);
            }
        });

        return v;
    }

    public interface MarkHolderListener {
        void showSnackbar(String s);

        void openMarkUpdate(Mark mark);

        Resources getResources();
    }
}
