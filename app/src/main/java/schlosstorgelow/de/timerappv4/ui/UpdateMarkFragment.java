package schlosstorgelow.de.timerappv4.ui;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.Mark;

public class UpdateMarkFragment extends Fragment {
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private EditText name;
    private EditText value;
    private EditText weight;
    private EditText date;
    private ImageButton datepicker;
    private Button save;
    private Button delete;
    private Date dateToSave;
    private Mark markToUpdate;

    private Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener today = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            dateToSave = changeDate(year, monthOfYear, dayOfMonth);
            date.setText(dateFormat.format(dateToSave));
        }

    };

    public UpdateMarkFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_update_mark, container, false);
        name = (EditText) v.findViewById(R.id.update_mark_editText_name);
        value = (EditText) v.findViewById(R.id.update_mark_editText_value);
        weight = (EditText) v.findViewById(R.id.update_mark_editText_weight);
        date = (EditText) v.findViewById(R.id.update_mark_editText_date);
        datepicker = (ImageButton) v.findViewById(R.id.datepicker_update);
        save = (Button) v.findViewById(R.id.update_mark_save);
        delete = (Button) v.findViewById(R.id.update_mark_delete);

        markToUpdate = (Mark) getArguments().getSerializable("mark");

        name.setText(markToUpdate.getName());
        value.setText(String.valueOf(markToUpdate.getValue()));
        weight.setText(String.valueOf(markToUpdate.getWeight()));
        date.setText(dateFormat.format(markToUpdate.getDate()));
        dateToSave = markToUpdate.getDate();
        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getContext(), today, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getActivity())
                        .title("Delete Mark " + markToUpdate.getName())
                        .content("Are you sure you want to delete this mark?")
                        .positiveText("OK!")
                        .negativeText("Cancel!")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                if (DBHelper.deleteMark(markToUpdate)) {
                                    new MaterialDialog.Builder(getActivity())
                                            .title("Delete Successful!")
                                            .content(markToUpdate.getName() + " was deleted!")
                                            .positiveText("OK!")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                                    getActivity().onBackPressed();
                                                }
                                            })
                                            .show();
                                }
                            }
                        })
                        .show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateDate() && validateName() && validateValue() && validateWeight()) {
                    markToUpdate.setDate(dateToSave);
                    markToUpdate.setValue(Double.parseDouble(value.getText().toString()));
                    markToUpdate.setWeight(Double.parseDouble(weight.getText().toString()));
                    markToUpdate.setName(name.getText().toString());
                    if (DBHelper.updateMark(markToUpdate)) {
                        new MaterialDialog.Builder(getActivity())
                                .title("Sucessfull updated!")
                                .content(markToUpdate.getName() + " was updated!")
                                .positiveText("OK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog dialog, DialogAction which) {
                                        getActivity().onBackPressed();
                                    }
                                })
                                .show();
                    }
                }
            }
        });

        return v;
    }

    private Date changeDate(int year, int monthOfYear, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        return myCalendar.getTime();
    }

    private boolean validateDate() {
        boolean inputOK = false;
        if (date.getText().length() == 0) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Date cant be empty!")
                    .positiveText("OK!")
                    .show();
        } else {
            try {
                Date input = dateFormat.parse(date.getText().toString());
                dateToSave = input;
                inputOK = true;
            } catch (ParseException e) {
                new MaterialDialog.Builder(getActivity())
                        .title("Input Error")
                        .content("Please enter Date in the format dd/MM/yyyy")
                        .positiveText("OK!")
                        .show();
            }
        }
        return inputOK;
    }

    public boolean validateWeight() {
        boolean inputOK = false;
        double input = 0;
        if (weight.getText().length() == 0) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Weight cant be empty!")
                    .positiveText("OK!")
                    .show();
        } else {
            input = Double.parseDouble(weight.getText().toString());
            if (input <= 0) {
                new MaterialDialog.Builder(getActivity())
                        .title("Input Error")
                        .content("Weight must be greater than zero!")
                        .positiveText("OK!")
                        .show();
            } else {
                inputOK = true;
            }
        }


        return inputOK;

    }

    public boolean validateValue() {
        boolean inputOK = false;
        double input = 0;
        if (value.getText().length() == 0) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Value cant be empty!")
                    .positiveText("OK!")
                    .show();
        } else {
            input = Double.parseDouble(value.getText().toString());
            if (input > 15 || input < 0) {
                new MaterialDialog.Builder(getActivity())
                        .title("Input Error")
                        .content("Value has to be an whole number between 1 and 15")
                        .positiveText("OK!")
                        .show();
            } else {
                inputOK = true;
            }
        }


        return inputOK;

    }

    public boolean validateName() {
        boolean inputOK = false;
        String input = name.getText().toString();
        if (input.length() == 0) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Name cant be empty!")
                    .positiveText("OK!")
                    .show();
        } else if (input.length() > 40) {
            new MaterialDialog.Builder(getActivity())
                    .title("Input Error")
                    .content("Name cant be longer tham 40 Symbols!")
                    .positiveText("OK!")
                    .show();
        } else {
            inputOK = true;
        }

        return inputOK;
    }
}
