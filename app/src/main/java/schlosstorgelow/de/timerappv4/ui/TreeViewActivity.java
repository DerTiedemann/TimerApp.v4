package schlosstorgelow.de.timerappv4.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.DBObject;
import schlosstorgelow.de.timerappv4.obj.Group;
import schlosstorgelow.de.timerappv4.obj.Mark;
import schlosstorgelow.de.timerappv4.obj.Subject;

public class TreeViewActivity extends AppCompatActivity implements GroupHolder.GroupHolderListener, AddHolder.AddHolderListener, MarkHolder.MarkHolderListener {

    TreeNode main_root;
    private Subject parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        parent = (Subject) getIntent().getSerializableExtra("parent");
        setTitle(parent.getName());
        refresh();
    }

    @Override
    protected void onResume() {
        refresh();
        super.onResume();
    }

    private void refresh() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.content);
        layout.removeAllViews();
        main_root = TreeNode.root();
        addGroupToRoot(parent, main_root);
        AndroidTreeView tr = new AndroidTreeView(this, main_root);
        layout.addView(tr.getView());
        DBHelper.updateSubjectAverage(parent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addGroupToRoot(DBObject p, TreeNode root) {
        for (Group g : DBHelper.readGroupsByParentID(p.getHierarchyId())) {
            TreeNode sub = new TreeNode(g).setViewHolder(new GroupHolder(this));
            root.addChild(sub);
            addGroupToRoot(g, sub);
        }
        for (Mark m : DBHelper.readMarksByParentID(p.getHierarchyId())) {
            root.addChild(new TreeNode(m).setViewHolder(new MarkHolder(this)));
        }
        root.addChild(new TreeNode(p).setViewHolder(new AddHolder(this)));
    }

    @Override
    public void showSnackbar(String s) {
        Snackbar.make(this.findViewById(R.id.content), s, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void openMarkUpdate(Mark mark) {
        Intent i = new Intent(this, UpdateActivity.class);
        i.putExtra("type", 0);
        i.putExtra("mark", mark);
        startActivity(i);
    }

    @Override
    public void openGroupUpdate(Group group) {
        Intent i = new Intent(this, UpdateActivity.class);
        i.putExtra("type", 1);
        i.putExtra("group", group);
        startActivity(i);
    }
}
