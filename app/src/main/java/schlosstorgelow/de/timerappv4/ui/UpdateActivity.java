package schlosstorgelow.de.timerappv4.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.obj.Group;
import schlosstorgelow.de.timerappv4.obj.Mark;

public class UpdateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Mark = 0
        //Group = 1
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        int type = getIntent().getIntExtra("type", 0);
        if (type == 0) {
            Mark mark = (Mark) getIntent().getSerializableExtra("mark");
            UpdateMarkFragment markFragment = new UpdateMarkFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("mark", mark);
            markFragment.setArguments(bundle);
            ft.add(R.id.update_content, markFragment);
            setTitle("Updateing " + mark.getName());
            ft.commit();

        } else {
            Group group = (Group) getIntent().getSerializableExtra("group");
            UpdateGroupFragment groupFragment = new UpdateGroupFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("group", group);
            groupFragment.setArguments(bundle);
            ft.add(R.id.update_content, groupFragment);
            setTitle("Updateing " + group.getName());
            ft.commit();
        }
    }
}
