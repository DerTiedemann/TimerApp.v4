package schlosstorgelow.de.timerappv4.obj;

/**************************************************************************************************
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 **************************************************************************************************/


import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;

/**
 * Subject
 * This class represents a Subject the User has at school
 * It contains different groups used to calculate the average of the Subject
 * For further information how groups work have a look at the doc of the Category-class {@link Group}
 * Subjects are the level 1 instance of the calculation algorithm/hierarchy
 * <p/>
 * Created by Jan Max Tiedemann on 23.06.2015.
 *
 * @author Jan Max Tiedemann
 * @version 1.0
 */

public class Subject implements Comparable<Subject>, Serializable, DBObject {

    private long id;
    private long hierarchyId;
    private String name;
    private double average;

    //CONSTRUCTOR

    public Subject(String name) {
        this.name = name;
    }

    public Subject() {
    }

    //GETTERS & SETTERS

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getHierarchyId() {
        return hierarchyId;
    }

    public void setHierarchyId(long hierarchyId) {
        this.hierarchyId = hierarchyId;
    }

    @Override
    public long getParentId() {
        return -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    //FUNCTIONS

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int compareTo(Subject another) {
        return name.compareTo(another.getName()) == 0 ? Long.compare(this.getId(), another.getId()) : name.compareTo(another.getName());
    }
}
