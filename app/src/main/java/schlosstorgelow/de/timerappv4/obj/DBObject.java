package schlosstorgelow.de.timerappv4.obj;

import java.io.Serializable;

/**
 * Created by Jan Max on 14.12.2015.
 */
public interface DBObject extends Serializable {
    long getId();

    long getHierarchyId();

    long getParentId();

    String getName();

}
