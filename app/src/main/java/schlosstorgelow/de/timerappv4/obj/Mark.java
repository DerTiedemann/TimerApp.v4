package schlosstorgelow.de.timerappv4.obj;

import android.annotation.SuppressLint;

import java.io.Serializable;
import java.util.Date;

/**************************************************************************************************
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 **************************************************************************************************/

public class Mark implements Comparable<Mark>, Serializable, DBObject {

    private long id;
    private long hierarchyId;
    private long parentId;
    private String name;
    private double value;
    private double weight;
    private Date date;

    //CONSTRUCTOR

    public Mark(long parentId, String name, double value, double weight, Date date) {
        this.parentId = parentId;
        this.name = name;
        this.value = value;
        this.weight = weight;
        this.date = date;
    }

    public Mark() {
    }

    //GETTERS & SETTERS

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getHierarchyId() {
        return hierarchyId;
    }

    public void setHierarchyId(long hierarchyId) {
        this.hierarchyId = hierarchyId;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @SuppressLint("NewApi")
    @Override
    public int compareTo(Mark another) {
        return name.compareTo(another.getName()) == 0 ? Long.compare(this.getId(), another.getId()) : name.compareTo(another.getName());
    }

    //BASIC FUNCTIONS


}
