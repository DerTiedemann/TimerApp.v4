package schlosstorgelow.de.timerappv4.db;

/**************************************************************************************************
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 **************************************************************************************************/

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import schlosstorgelow.de.timerappv4.obj.Group;
import schlosstorgelow.de.timerappv4.obj.Mark;
import schlosstorgelow.de.timerappv4.obj.Subject;
import schlosstorgelow.de.timerappv4.ui.MainActivity;

/**
 * Created by Jan Max on 08.10.2015.
 */
public final class DBHelper {
    //Basic Functions
    private static final DateFormat dateFormat_db = new SimpleDateFormat("yyyy-MM-dd");
    private static DBConnector DBC;
    private static SQLiteDatabase database;

    public static void open(Context ctx) {
        DBC = new DBConnector(ctx);
        database = DBC.getWritableDatabase();
    }

    protected static void close() {
        if (database != null) {
            DBC.close();
        }
    }

    public static SQLiteDatabase getDB() {
        return database;
    }

    public static double getGlobalAvg() {
        List<Subject> subjects = readAllSubjects();
        double total = 0;
        for (Subject s : subjects) {
            total += s.getAverage();
        }
        return total / subjects.size();
    }

    public static void updateSubjectAverage(Subject s) {
        s.setAverage(calcAvg(s.getHierarchyId()));
        updateSubject(s);
    }

    public static double calcAvg(long hierarchyId) {
        List<Mark> marks = readMarksByParentID(hierarchyId);
        List<Group> groups = readGroupsByParentID(hierarchyId);

        double totalWeight = 0;
        double totalValue = 0;

        for (Group g : groups) {
            totalWeight += g.getWeight();
            totalValue += (calcAvg(g.getHierarchyId())) * g.getWeight();
        }

        for (Mark m : marks) {
            totalWeight += m.getWeight();
            totalValue += m.getValue() * m.getWeight();
        }

        if (Double.isNaN(totalValue / totalWeight)) {
            return 0;
        } else {
            return totalValue / totalWeight;
        }
    }

    // CRUD Framework implementation

    // Subject Section

    // Cursor to Subject
    private static Subject ctos(@NotNull Cursor c) {
        Subject s = new Subject();
        s.setId(c.getLong(SubjectTable.COL_NO_ID));
        s.setName(c.getString(SubjectTable.COL_NO_NAME));
        s.setAverage(c.getDouble(SubjectTable.COL_NO_AVERAGE));
        return s;
    }

    // Create Section

    public static void createSubject(@NotNull Subject s) {
        // Adding Subject to Subject Table
        ContentValues values = new ContentValues();
        values.put(SubjectTable.COLUMN_NAME, s.getName());
        values.put(SubjectTable.COLUMN_AVERAGE, s.getAverage());
        s.setId(database.insert(SubjectTable.TABLE, null, values));
        values.clear();

        // Creating Hierarchy Entry
        values.put(HierarchyTable.COLUMN_TYPE, HierarchyTable.TYPE_SUBJECT);
        values.put(HierarchyTable.COLUMN_RELATED_ID, s.getId());
        values.putNull(HierarchyTable.COLUMN_PARENT_ID);
        s.setHierarchyId(database.insert(HierarchyTable.TABLE, null, values));
        values.clear();
    }

    // Read Section

    // Everything
    public static List<Subject> readAllSubjects() {
        Cursor c = database.query(HierarchyTable.TABLE,
                HierarchyTable.ALL_COLUMNS,
                HierarchyTable.COLUMN_TYPE + "=" + HierarchyTable.TYPE_SUBJECT,
                null, null, null, null);

        List<Subject> sl = new ArrayList<>();


        while (c.moveToNext()) {
            Cursor cs = database.query(SubjectTable.TABLE,
                    SubjectTable.ALL_COLUMNS,
                    SubjectTable.COLUMN_ID + "=" + c.getLong(HierarchyTable.COL_NO_RELATED_ID),
                    null, null, null, null);
            cs.moveToFirst();
            Subject s = ctos(cs);
            s.setHierarchyId(c.getLong(HierarchyTable.COL_NO_ID));
            sl.add(s);
            cs.close();
        }
        Collections.sort(sl);
        c.close();
        return sl;
    }

    // Update Section

    public static boolean updateSubject(@NotNull Subject s) {
        ContentValues values = new ContentValues();
        values.put(SubjectTable.COLUMN_NAME, s.getName());
        values.put(SubjectTable.COLUMN_AVERAGE, s.getAverage());

        return database.update(SubjectTable.TABLE, values,
                SubjectTable.COLUMN_ID + "=" + Long.toString(s.getId()), null) > 0;
    }

    // Delete Section

    public static boolean deleteSubject(@NotNull Subject s) {
        return database.delete(SubjectTable.TABLE,
                SubjectTable.COLUMN_ID + "=" + Long.toString(s.getId()), null) > 0 &&
                database.delete(HierarchyTable.TABLE, HierarchyTable.COLUMN_ID + "=" +
                        Long.toString(s.getHierarchyId()), null) > 0 && deleteEverythingByParentId(s.getHierarchyId());
    }

    private static boolean deleteEverythingByParentId(long parentId) {
        return deleteGroupByParentId(parentId) && deleteMarkByParentId(parentId);
    }

    // Group section

    // Cursor to Group
    private static Group ctog(@NotNull Cursor c) {
        Group g = new Group();
        g.setId(c.getLong(GroupTable.COL_NO_ID));
        g.setName(c.getString(GroupTable.COL_NO_NAME));
        g.setWeight(c.getDouble(GroupTable.COL_NO_WEIGHT));
        return g;
    }

    // Create Section
    public static void createGroup(@NotNull Group g) {
        // Adding Subject to Group Table
        ContentValues values = new ContentValues();
        values.put(GroupTable.COLUMN_NAME, g.getName());
        values.put(GroupTable.COLUMN_WEIGHT, g.getWeight());
        g.setId(database.insert(GroupTable.TABLE, null, values));

        // Creating Hierarchy Entry
        values.clear();
        values.put(HierarchyTable.COLUMN_TYPE, HierarchyTable.TYPE_GROUP);
        values.put(HierarchyTable.COLUMN_RELATED_ID, g.getId());
        values.put(HierarchyTable.COLUMN_PARENT_ID, g.getParentId());
        g.setHierarchyId(database.insert(HierarchyTable.TABLE, null, values));
        values.clear();
    }

    // Read Section
    public static List<Group> readGroupsByParentID(long parentID) {

        Cursor c = database.query(HierarchyTable.TABLE,
                HierarchyTable.ALL_COLUMNS,
                HierarchyTable.COLUMN_TYPE + "=" + HierarchyTable.TYPE_GROUP + " AND " + HierarchyTable.COLUMN_PARENT_ID + "=" + Long.toString(parentID),
                null, null, null, null);

        Log.d(MainActivity.TAG, "Group Querry for PID: " + parentID);
        List<Group> gl = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                Cursor cg = database.query(GroupTable.TABLE,
                        null,
                        GroupTable.COLUMN_ID + "=" + c.getLong(HierarchyTable.COL_NO_RELATED_ID),
                        null, null, null, null);
                if (cg.moveToFirst()) {
                    Group g = ctog(cg);
                    g.setParentId(parentID);
                    g.setHierarchyId(c.getLong(HierarchyTable.COL_NO_ID));
                    Log.d(MainActivity.TAG, "Name: " + g.getName() + " HID: " + g.getHierarchyId() + " ID: " + g.getId() + " PID: " + g.getParentId());
                    gl.add(g);
                }
                cg.close();
            } while (c.moveToNext());
            Collections.sort(gl);
        }
        c.close();

        return gl;
    }

    // Update Section
    public static boolean updateGroup(@NotNull Group g) {
        ContentValues values = new ContentValues();
        values.put(GroupTable.COLUMN_NAME, g.getName());
        values.put(GroupTable.COLUMN_WEIGHT, g.getWeight());
        return database.update(GroupTable.TABLE, values, GroupTable.COLUMN_ID + "=" +
                Long.toString(g.getId()), null) > 0;
    }

    // Delete Section
    public static boolean deleteGroup(@NotNull Group g) {
        Log.d(MainActivity.TAG, "Deleting Group :" + g.getName() + " with ID:" + g.getId());
        return deleteGroupById(g.getId());
    }

    private static boolean deleteGroupById(long groupId) {
        return database.delete(GroupTable.TABLE, GroupTable.COLUMN_ID + "=" +
                Long.toString(groupId), null) > 0 && deleteMarkByParentId(groupId) &&
                database.delete(HierarchyTable.TABLE, HierarchyTable.COLUMN_ID + "=" +
                        Long.toString(groupId), null) > 0;
    }

    private static boolean deleteGroupByParentId(long parentId) {
        boolean success = true;
        Log.d(MainActivity.TAG, "Deleting all Groups from parent with ID:" + String.valueOf(parentId));
        Cursor c = database.query(HierarchyTable.TABLE,
                HierarchyTable.ALL_COLUMNS,
                HierarchyTable.COLUMN_TYPE + "=" + HierarchyTable.TYPE_GROUP + " AND " +
                        HierarchyTable.COLUMN_PARENT_ID + "=" + Long.toString(parentId),
                null, null, null, null);
        c.moveToFirst();

        while (c.moveToNext()) {
            if (!(deleteGroupByParentId(c.getLong(HierarchyTable.COL_NO_ID))) &&
                    deleteGroupById(c.getLong(HierarchyTable.COL_NO_RELATED_ID))) success = false;
        }
        return success;
    }

    // Mark Section

    // Cursor to Mark
    private static Mark ctom(@NotNull Cursor c) {
        Mark m = new Mark();
        m.setId(c.getLong(MarkTable.COL_NO_ID));
        m.setName(c.getString(MarkTable.COL_NO_NAME));
        m.setValue(c.getDouble(MarkTable.COL_NO_VALUE));
        m.setWeight(c.getDouble(MarkTable.COL_NO_WEIGHT));
        try {
            m.setDate(dateFormat_db.parse(c.getString(MarkTable.COL_NO_DATE)));
        } catch (ParseException e) {
            return null;
        }
        return m;
    }

    // Create Section
    public static void createMark(@NotNull Mark m) {
        String date = dateFormat_db.format(m.getDate());
        ContentValues values = new ContentValues();
        values.put(MarkTable.COLUMN_NAME, m.getName());
        values.put(MarkTable.COLUMN_VALUE, m.getValue());
        values.put(MarkTable.COLUMN_WEIGHT, m.getWeight());
        values.put(MarkTable.COLUMN_DATE, date);
        m.setId(database.insert(MarkTable.TABLE, null, values));
        values.clear();

        values.put(HierarchyTable.COLUMN_TYPE, HierarchyTable.TYPE_MARK);
        values.put(HierarchyTable.COLUMN_RELATED_ID, m.getId());
        values.put(HierarchyTable.COLUMN_PARENT_ID, m.getParentId());
        m.setHierarchyId(database.insert(HierarchyTable.TABLE, null, values));
        values.clear();
    }

    // Read Section

    public static List<Mark> readMarksByParentID(long parentID) {

        Cursor c = database.query(HierarchyTable.TABLE,
                HierarchyTable.ALL_COLUMNS,
                HierarchyTable.COLUMN_TYPE + "=" + HierarchyTable.TYPE_MARK + " AND " + HierarchyTable.COLUMN_PARENT_ID + "=" + Long.toString(parentID),
                null, null, null, null);

        List<Mark> ml = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                Cursor cm = database.query(MarkTable.TABLE,
                        null,
                        MarkTable.COLUMN_ID + "=" + c.getLong(HierarchyTable.COL_NO_RELATED_ID),
                        null, null, null, null);
                if (cm.moveToFirst()) {
                    Mark m = ctom(cm);
                    m.setParentId(parentID);
                    m.setHierarchyId(c.getLong(HierarchyTable.COL_NO_ID));
                    Log.d(MainActivity.TAG, "Name: " + m.getName() + " HID: " + m.getHierarchyId() + " ID: " + m.getId() + " PID: " + m.getParentId());
                    ml.add(m);
                }
                cm.close();
            } while (c.moveToNext());
            Collections.sort(ml);
        }
        c.close();
        return ml;
    }

    // Update Section

    public static boolean updateMark(@NotNull Mark m) {
        ContentValues values = new ContentValues();
        values.put(MarkTable.COLUMN_NAME, m.getName());
        values.put(MarkTable.COLUMN_VALUE, m.getValue());
        values.put(MarkTable.COLUMN_WEIGHT, m.getWeight());
        values.put(MarkTable.COLUMN_DATE, dateFormat_db.format(m.getDate()));

        return database.update(MarkTable.TABLE, values, MarkTable.COLUMN_ID + "=" +
                Long.toString(m.getId()), null) > 0;
    }

    // Delete Section

    public static boolean deleteMark(@NotNull Mark m) {
        Log.d(MainActivity.TAG, "Deleting Mark: " + m.getName() + " with ID:" + m.getId());
        return database.delete(MarkTable.TABLE, MarkTable.COLUMN_ID + "=" +
                Long.toString(m.getId()), null) > 0;
    }

    private static boolean deleteMarkByParentId(long parentId) {
        boolean success = true;
        Log.d(MainActivity.TAG, "Deleting all Marks from parent with ID:" + String.valueOf(parentId));
        Cursor c = database.query(HierarchyTable.TABLE,
                HierarchyTable.ALL_COLUMNS,
                HierarchyTable.COLUMN_TYPE + "=" + HierarchyTable.TYPE_MARK + " AND " +
                        HierarchyTable.COLUMN_PARENT_ID + "=" + Long.toString(parentId),
                null, null, null, null);
        c.moveToFirst();

        while (c.moveToNext()) {
            if (!(database.delete(MarkTable.TABLE, MarkTable.COLUMN_ID + "=" +
                    Long.toString(c.getLong(HierarchyTable.COL_NO_RELATED_ID)), null) > 0)) {
                success = false;
            }
        }
        return success;
    }
}